# Description

This plugin render css for grid inspired by bootstrap.

# Examples for sass

## Variables

```scss
$grid-lg-min: 1200px;
$grid-md-max: 1199px;
$grid-md-min: 992px;
$grid-sm-max: 991px;
$grid-sm-min: 768px;
$grid-xs-max: 767px;
```

## Fix

```scss
*,
:before,
:after {
  box-sizing: border-box;
}
```

## Render Container

```scss
.container {
  create-container: 100%;
  gutter: 30;
}
```

## Render Row

```scss
.row {
  create-row: 0;
  gutter: 30;
}
```

## Render Columns .col-xs-1..12

```scss
.col-xs {
  create-columns: 12;
  gutter: 30;
}
```

## Render Mediaquery for lg

```scss
@media screen and (min-width: $grid-lg-min) {
  .container {
    create-container: $grid-lg-min;
    gutter: 30;
  }

  .col-lg {
    create-columns: 12;
    gutter: 30;
  }
}
```

## Render Mediaquery for md

```scss
@media screen and (max-width: $grid-md-max) and (min-width: $grid-md-min) {
  .container {
    create-container: $grid-md-max;
    gutter: 30;
  }

  .col-md {
    create-columns: 12;
    gutter: 30;
  }
}
```

## Render Mediaquery for sm

```scss
@media screen and (max-width: $grid-sm-max) and (min-width: $grid-sm-min) {
  .container {
    create-container: $grid-sm-max;
    gutter: 30;
  }

  .col-sm {
    create-columns: 12;
    gutter: 30;
  }
}
```

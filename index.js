// @flow

const postcss = require('postcss');

const DEFAULT_OPTIONS = {
  gutter: 30,
};

function renderColumnSelector(original, index, before) {
  return `${before}${original}-${index}`;
}

module.exports = postcss.plugin('postcss-bootstrap-grid', opts => {
  const options = {
    ...DEFAULT_OPTIONS,
    ...opts,
  };

  const getContainerCss = (width, selector, gutter, raws) => {
    const padding = (gutter || options.gutter) / 2;
    const before = (raws.before || '').replace('  ', '');

    const css = [
      `width: ${width};`,
      `padding-left: ${padding}px;`,
      `padding-right: ${padding}px;`,
      'margin-left: auto;',
      'margin-right: auto;',
    ].join(`${before}\t`);

    return before + [`${selector} {${before}\t${css} }`].join(`${before}\t`);
  };

  const getRowCss = (selector, gutter, raws) => {
    const margin = (gutter || options.gutter) / 2;
    const before = (raws.before || '').replace('  ', '');

    const css = [
      `margin-left: ${margin}px;`,
      `margin-right: ${margin}px;`,
    ].join(`${before}\t`);

    const afterCss = [`content: " ";`, `display: block;`].join(`${before}\t`);

    const beforeCss = [`clear: both;`].join(`${before}\t`);

    return [
      `${before}${selector} {${before}\t${css} }`,
      `${before}${selector}:after,\n${selector}:before {${before}\t${afterCss} }`,
      `${before}${selector}:after {${before}\t${beforeCss} }`,
    ].join('\n\n');
  };

  const getColCss = (cols, selector, before) => {
    return Array.from(Array(parseInt(cols)))
      .map((empty, col) => {
        const width = parseFloat(100 / cols * (col + 1)).toFixed(2);

        return `${renderColumnSelector(
          selector,
          col + 1,
          before
        )} {${before}\twidth: ${width}%; }`;
      })
      .join(`${before}\t`);
  };

  const getComonColCss = (cols, selector, gutter, before) => {
    const padding = (gutter || options.gutter) / 2;

    const comonCss = ` {${before}\t${[
      'float: left;',
      `padding-left: ${padding}px;`,
      `padding-right: ${padding}px;`,
    ].join(`${before}\t`)} }`;

    return (
      before +
      Array.from(Array(parseInt(cols)))
        .map(
          (empty, col) =>
            `${renderColumnSelector(
              selector,
              col + 1,
              col + 1 === 1 ? before : before.replace('\n', '')
            )}`
        )
        .join(',\n') +
      comonCss
    );
  };

  const prepareGutter = rule => {
    const gutter = rule.nodes.find(node => node.prop === 'gutter');

    return gutter ? parseInt(gutter) : options.gutter;
  };

  return root => {
    root.walkRules(rule => {
      rule.walkDecls('create-container', function(decl) {
        const { selector } = decl.parent;

        rule.replaceWith(
          postcss.parse(
            getContainerCss(
              decl.value,
              selector,
              prepareGutter(rule),
              decl.raws
            )
          )
        );
      });

      rule.walkDecls('create-row', function(decl) {
        rule.replaceWith(
          postcss.parse(
            getRowCss(decl.parent.selector, prepareGutter(rule), decl.raws)
          )
        );
      });

      rule.walkDecls('create-columns', function(decl) {
        const { selector } = decl.parent;
        const cols = parseInt(decl.value);
        const before = (decl.raws.before || '').replace('  ', '');

        rule.replaceWith(
          postcss.parse(
            [
              getComonColCss(cols, selector, prepareGutter(rule), before),
              getColCss(cols, selector, before),
            ].join(`${before}\t`)
          )
        );
      });
    });
  };
});
